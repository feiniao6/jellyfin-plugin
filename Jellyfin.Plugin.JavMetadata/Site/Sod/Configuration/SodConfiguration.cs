﻿using Jellyfin.Plugin.JavMetadata.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfin.Plugin.JavMetadata.Site.Sod.Configuration
{
    public class SodConfiguration: DefaultConfiguration
    {
        /// <summary>
        /// 识别码正则
        /// </summary>
        public string AvidPattern { get; set; }

        /// <summary>
        /// 标题正则
        /// </summary>
        public string TitlePattern { get; set; }

        /// <summary>
        /// 预览图列表正则
        /// </summary>
        public string ScreenshotListPattern { get; set; }

        /// <summary>
        /// 预览图正则
        /// </summary>
        public string ScreenshotPattern { get; set; }

        /// <summary>
        /// 简介正则
        /// </summary>
        public string IntroPattern { get; set; }

        /// <summary>
        /// 预告片正则
        /// </summary>
        public string TrailerPattern { get; set; }

        public SodConfiguration()
        {

            // set default options here
            ProviderId = $"SOD Prime Id";
            ProviderName = "SOD Prime";
            Domain = "ec.sod.co.jp/prime";
            SearchResultPattern = @"videos_s_mainbox""[\w\W]*?img\ssrc=""(?<poster>.*?)""[\w\W]*?id=(?<avid>.*?)"">(?<title>.*?)</a>[\w\W]*?発売日\s(?<date>.*?)<br>";
            CoverPattern = @"videos_samimg"">\s*<a\shref=""(?<fanart>.*?)""";
            AvidPattern = @"品番</td>[\w\W]*?>(?<avid>.*?)</";
            TitlePattern = "<h1>(?<title>.*?)</h1>";
            ReleaseDatePattern = @"発売年月日</td>[\w\W]*?>(?<date>.*?)</";
            DurationPattern = @"再生時間</td>[\w\W]*?>(?<duration>\d*)";
            DirectorListPattern = @"監督</td>[\w\W]*?>(?<directors>.*?)</td";
            DirectorPattern = "href=(?<url>.*?)>(?<name>.*?)<";
            StudioListPattern = @"メーカー</td>[\w\W]*?>(?<studios>.*?)</td";
            StudioPattern = @"href=""(?<url>.*?)"">(?<name>.*?)</";
            LabelListPattern = string.Empty; //无
            LabelPattern = @"レーベル</td>[\w\W]*?>(?<label>.*?)</td";
            CollectionListPattern = @"シリーズ名</td>[\w\W]*?_tx"">\s*(?<series>.*?)</td";
            CollectionPattern = "href=(?<url>.*?)>(?<name>.*?)<"; // URL: ../videos/genre/?series[]=690
            GenreListPattern = @"ジャンル</td>[\w\W]*?_tx"">\s*(?<geners>.*?)</td";
            GenrePattern = @"href=""(?<url>.*?)"">(?<name>.*?)</";
            ActressListPattern = @"出演者</td>[\w\W]*?_tx"">\s*(?<actresses>.*?)</td";
            ActressPattern = "href=(?<url>.*?)>(?<name>.*?)<";
            ScreenshotListPattern = @"</h3>(?<screenshots>[\w\W]*?)<div class=""clear"">";
            ScreenshotPattern = @"href=""(?<url>.*?)""";
            IntroPattern = @"<article>\s*(?<intro>.*?)\s*</article>";
            TrailerPattern = string.Empty;
        }
    }
}

/*
搜索页： https://ec.sod.co.jp/prime/videos/genre/?search_type=1&sodsearch=STARS-527
详情页：https://ec.sod.co.jp/prime/videos/?id=STARS-527

https://dy43ylo5q3vt8.cloudfront.net/_sample/202202/stars_527/stars_527_sample.mp4
https://dy43ylo5q3vt8.cloudfront.net/_pics/202202/stars_527/stars_527_m.jpg

 
 */
