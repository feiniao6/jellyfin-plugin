﻿using Jellyfin.Plugin.JavMetadata.Site.Sod.Service;
using MediaBrowser.Common.Plugins;
using Microsoft.Extensions.DependencyInjection;

namespace Jellyfin.Plugin.JavMetadata.Site.Sod
{
    /// <summary>
    /// Register services.
    /// </summary>
    public class ServiceRegistrator : IPluginServiceRegistrator
    {
        /// <inheritdoc />
        public void RegisterServices(IServiceCollection serviceCollection)
        {
            // 注入
            serviceCollection.AddSingleton<SodHttpService>();
        }
    }
}
