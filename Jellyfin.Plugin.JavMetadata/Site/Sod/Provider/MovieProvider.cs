﻿using Jellyfin.Plugin.JavMetadata.Dto;
using Jellyfin.Plugin.JavMetadata.Site.Sod.Service;
using MediaBrowser.Controller.Entities.Movies;
using MediaBrowser.Controller.Providers;
using MediaBrowser.Model.Entities;
using MediaBrowser.Model.Providers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfin.Plugin.JavMetadata.Site.Sod.Provider
{
    public class MovieProvider : IHasOrder, IRemoteMetadataProvider<Movie, MovieInfo>
    {
        private readonly ILogger<MovieProvider> _logger;
        private readonly SodHttpService _http;

        public string Name => Plugin.Instance.Configuration.Sod.ProviderName;

        public int Order => 3;

        public MovieProvider(ILogger<MovieProvider> logger, SodHttpService httpService)
        {
            _logger = logger;
            _http = httpService;
        }

        public async Task<HttpResponseMessage> GetImageResponse(string url, CancellationToken cancellationToken) => await _http.GetResponseAsync(url, cancellationToken);

        public async Task<MetadataResult<Movie>> GetMetadata(MovieInfo info, CancellationToken cancellationToken)
        {
            // 读取 AvMoo Id
            var id = info.GetProviderId(Plugin.Instance.Configuration.Sod.ProviderId);

            // 如果 AvMoo Id 为空，则根据标题重新获取，且默认使用结果的第一条数据
            if (string.IsNullOrWhiteSpace(id))
            {
                //var results = await GetIdsAsync(info.Name, cancellationToken);
                var results = (await _http.SearchAsync<SearchResult>(info.Name, cancellationToken).ConfigureAwait(false)).Select(i => i.Id);

                if (results.Count() > 0)
                {
                    id = results.FirstOrDefault();
                }
                else
                {
                    return new MetadataResult<Movie>();
                }
            }

            // 获取 元数据
            var movie = await _http.GetMovieMetadataAsync(id, cancellationToken).ConfigureAwait(false);

            if (movie != null && movie.HasMetadata)
            {
                // 如果能获取到元数据，则把 AvMoo Id 设置为 当前 id
                info.SetProviderId(Plugin.Instance.Configuration.Sod.ProviderId, id);
            }

            return movie;
        }

        public async Task<IEnumerable<RemoteSearchResult>> GetSearchResults(MovieInfo searchInfo, CancellationToken cancellationToken)
        {
            var results = new List<RemoteSearchResult>();
            var ids = new List<string>();

            // 获取 Avmoo Id
            var AvmooId = searchInfo.GetProviderId(Plugin.Instance.Configuration.Sod.ProviderId);

            if (!string.IsNullOrEmpty(AvmooId))
            {
                // id 不为空，添加到 id 列表
                ids = new List<string>
                {
                    AvmooId
                };
            }
            else
            {
                // id 为空，则通过名称在线搜索并返回搜索结果的 id 列表
                //ids = (List<string>)await GetIdsAsync(searchInfo.Name, cancellationToken).ConfigureAwait(false);
                ids = (await _http.SearchAsync<SearchResult>(searchInfo.Name, cancellationToken).ConfigureAwait(false)).Select(i => i.Id).ToList();
                _logger.LogInformation($"search results count: {ids.Count}");
            }

            // 遍历 id 列表
            foreach (string id in ids)
            {
                // 获取影片详情
                var item = await _http.GetMovieAsync<MovieItem>(id, cancellationToken).ConfigureAwait(false);

                // 转换为 Jellyfin 查找结果(RemoteSearchResult)对象
                var result = new RemoteSearchResult()
                {
                    IndexNumber = searchInfo.IndexNumber,
                    Name = item.Title,
                    ImageUrl = item.Poster,
                    Overview = item.Intro,
                    ParentIndexNumber = searchInfo.ParentIndexNumber,
                    SearchProviderName = Name
                };

                // 如果发行日期不为空，则设置年份
                if (item.ReleaseDate != null)
                {
                    result.PremiereDate = item.ReleaseDate;
                    result.ProductionYear = item.ReleaseDate?.Year;
                }

                _logger.LogInformation($"ProviderId: {Plugin.Instance.Configuration.Sod.ProviderId}");

                // 设置 id
                result.SetProviderId(Plugin.Instance.Configuration.Sod.ProviderId, id);

                // 添加到搜索结果列表
                results.Add(result);
            }

            return results;
        }
    }
}
