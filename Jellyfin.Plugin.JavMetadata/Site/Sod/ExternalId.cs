﻿using MediaBrowser.Controller.Entities;
using MediaBrowser.Controller.Entities.Movies;
using MediaBrowser.Controller.Providers;
using MediaBrowser.Model.Entities;
using MediaBrowser.Model.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfin.Plugin.JavMetadata.Site.Sod
{
    public class ExternalId : IExternalId
    {
        public string ProviderName => Plugin.Instance.Configuration.Sod.ProviderName;

        public string Key => Plugin.Instance.Configuration.Sod.ProviderId;

        public ExternalIdMediaType? Type => null;

        public string UrlFormatString => $"https://{Plugin.Instance?.Configuration.Sod.Domain}/videos/?id={{0}}";

        public bool Supports(IHasProviderIds item)
        {
            return item is Movie || item is Video;
        }
    }
}
