﻿using Jellyfin.Plugin.JavMetadata.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfin.Plugin.JavMetadata.Site.Avsox.Dto
{
    public class AvsoxSearchResult : SearchResult
    {
        /// <summary>
        /// 识别码
        /// </summary>
        public string Avid { get; set; }

        /// <summary>
        /// 小封面
        /// </summary>
        public string Poster { get; set; }
    }
}
