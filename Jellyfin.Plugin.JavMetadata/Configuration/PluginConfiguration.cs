using Jellyfin.Plugin.JavMetadata.Site.Avmoo.Configuration;
using Jellyfin.Plugin.JavMetadata.Site.Avsox.Configuration;
using Jellyfin.Plugin.JavMetadata.Site.Sod.Configuration;
using MediaBrowser.Model.Plugins;

namespace Jellyfin.Plugin.JavMetadata.Configuration
{
    /// <summary>
    /// 默认设置
    /// </summary>
    public class DefaultConfiguration
    {
        /// <summary>
        /// 
        /// </summary>
        public string ProviderId { get; set; }

        public string ProviderName { get; set; }

        /// <summary>
        /// 域名
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// 搜索结果正则
        /// </summary>
        public string SearchResultPattern { get; set; }

        /// <summary>
        /// 大封面图正则
        /// </summary>
        public string CoverPattern { get; set; }

        /// <summary>
        /// 发行日期正则
        /// </summary>
        public string ReleaseDatePattern { get; set; }

        /// <summary>
        /// 时长正则
        /// </summary>
        public string DurationPattern { get; set; }

        /// <summary>
        /// 导演列表正则
        /// </summary>
        public string DirectorListPattern { get; set; }

        /// <summary>
        /// 导演正则
        /// </summary>
        public string DirectorPattern { get; set; }

        /// <summary>
        /// 工作室列表正则
        /// </summary>
        public string StudioListPattern { get; set; }

        /// <summary>
        /// 工作室正则
        /// </summary>
        public string StudioPattern { get; set; }

        /// <summary>
        /// 发行商列表正则
        /// </summary>
        public string LabelListPattern { get; set; }

        /// <summary>
        /// 发行商正则
        /// </summary>
        public string LabelPattern { get; set; }

        /// <summary>
        /// 系列列表正则
        /// </summary>
        public string CollectionListPattern { get; set; }

        /// <summary>
        /// 系列正则
        /// </summary>
        public string CollectionPattern { get; set; }

        /// <summary>
        /// 类别列表正则
        /// </summary>
        public string GenreListPattern { get; set; }

        /// <summary>
        /// 类别正则
        /// </summary>
        public string GenrePattern { get; set; }

        /// <summary>
        /// 演员列表正则
        /// </summary>
        public string ActressListPattern { get; set; }

        /// <summary>
        /// 演员正则
        /// </summary>
        public string ActressPattern { get; set; }

    }

    public class PluginConfiguration : BasePluginConfiguration
    {
        // store configurable settings your plugin might need

        public AvmooConfiguration Avmoo { get; set; }

        public AvsoxConfiguration Avsox { get; set; }

        public SodConfiguration Sod { get; set; }

        public PluginConfiguration()
        {
            Avmoo = new AvmooConfiguration();
            Avsox = new AvsoxConfiguration();
            Sod = new SodConfiguration();
        }
    }
}
